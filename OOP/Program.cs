﻿using OOP.StorageServices;
using OOP.UIServices;
using OOP.Interfaces;

namespace OOP;

public class Program
{
    public static void Main(string[] args)
    {
        IStorageService storageService = new FileStorageService();
        IUserInterface uiService = new ConsoleUI(storageService);
        uiService.StartProgram();
    }
}