﻿namespace OOP.Documents
{
    public class LocalizedBook : Book
    {
        public string CountryOfLocalization { get; set; }
        public string LocalPublisher { get; set; }

        public override string ToString()
        {
            return base.ToString() + $", \n Country of localization: {CountryOfLocalization}, \n Local publisher: {LocalPublisher}";
        }
    }
}
