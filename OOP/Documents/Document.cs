﻿using System.Reflection;
using System.Text;

namespace OOP.Documents
{
    public abstract class Document
    {
        public string Title { get; set; }
        public DateTime DatePublished { get; set; }
        public override string ToString()
        {
            return $"\n Title: {Title}, \n Date published: {DatePublished}";
        }
    }
}
