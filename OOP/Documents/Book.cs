﻿namespace OOP.Documents
{
    public class Book : Document
    {
        public string Author { get; set; }
        public string ISBN { get; set; }
        public int NumberOfPages { get; set; }
        public string Publisher { get; set; }
        public override string ToString()
        {
            return base.ToString() + $"\n Author: {Author} \n ISBN: {ISBN}, \n Number of pages: {NumberOfPages}, \n Publisher: {Publisher}";
        }
    }
}
