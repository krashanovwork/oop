﻿namespace OOP.Documents
{
    public class Magazine : Document
    {
        public int ReleaseNumber { get; set; }
        public string Publisher { get; set; }

        public override string ToString()
        {
            return base.ToString() + $", \n Publisher: {Publisher}, \n Release number: {ReleaseNumber}";
        }
    }
}
