﻿namespace OOP.Documents
{
    public class Patent : Document
    {
        public string Author { get; set; }
        public int UniqueId { get; set; }
        public DateTime ExpirationDate { get; set; }

        public override string ToString()
        {
            return base.ToString() + $", \n Author: {Author} \n Unique id: {UniqueId}, \n Expiration date: {ExpirationDate}";
        }
    }
}
