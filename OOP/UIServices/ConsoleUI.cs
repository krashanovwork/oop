﻿using OOP.Documents;
using OOP.Interfaces;

namespace OOP.UIServices
{
    public class ConsoleUI : IUserInterface
    {
        private readonly IStorageService _storageService;

        public ConsoleUI(IStorageService storageService)
        {
            _storageService = storageService;
        }

        public void DisplayDocument(Type typeOfDocument)
        {
            Console.Write($"\nEnter the ID number of the {typeOfDocument.Name}: ");
            var id = Console.ReadLine();

            var document = _storageService.SearchDocument(id!, typeOfDocument);

            if (document is not null)
            {
                Console.WriteLine(document);
                return;
            }

            Console.WriteLine($"{typeOfDocument.Name} with ID {id} does not exist.");
        }

        public void Exit()
        {
            Environment.Exit(0);
        }

        public void StartProgram()
        {
            do
            {
                Console.Clear();
                Console.WriteLine("Choose document type:");
                Console.WriteLine("1. Search books.");
                Console.WriteLine("2. Search localized books.");
                Console.WriteLine("3. Search patents.");
                Console.WriteLine("4. Search magazines.");
                Console.WriteLine("5. Quit.");
                Console.Write("- ");

                var response = Console.ReadLine();
                switch (response)
                {
                    case "1":
                        DisplayDocument(typeof(Book));
                        break;
                    case "2":
                        DisplayDocument(typeof(LocalizedBook));
                        break;
                    case "3":
                        DisplayDocument(typeof(Patent));
                        break;
                    case "4":
                        DisplayDocument(typeof(Magazine));
                        break;
                    case "5":
                        Exit();
                        break;
                };
                Console.ReadKey();
            } while (true);
        }
    }
}
