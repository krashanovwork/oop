﻿namespace OOP.Interfaces
{
    internal interface IUserInterface
    {
        void DisplayDocument(Type typeOfDocument);
        void StartProgram();
        void Exit();
    }
}
