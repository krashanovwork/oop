﻿using OOP.Documents;

namespace OOP.Interfaces
{
    public interface IStorageService
    {
        Document SearchDocument(string id, Type typeOfDocument);
    }
}
